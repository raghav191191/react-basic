import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './App.css';
import './Responsive.css';
import Header from './Header';
import Footer from './Footer';
import MainPage from './MainPage';
import userList from './component/userdata/userList';
import Login from './component/login/login';
import Register from './component/register/register';


class App extends Component {
  render() {
    return (
      <div className="App height100 ">

        <Router>
          <div>
            <Header />
            <hr />

            <Route exact path="/" component={MainPage} />
            <Route path="/user-list" component={userList} />
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
           
          </div>
        </Router>

      </div>
    );
  }
}

export default App;
