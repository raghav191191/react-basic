import React, { Component } from 'react';
import { Navbar, NavItem, Nav, Alert } from 'react-bootstrap'; 
import { Link } from 'react-router-dom';
import RegisterService from './serviecs/registerServiecs';

import Footer from './Footer';
const Reg = new RegisterService();
class MainPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {},
            errors: {},
            fname:'',
            email: '',
            message: '',
            showAlert:false,
            color:'',
            message:''

        };

       // this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    // Register form validations
    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        //Email
        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "Cannot be empty";
        }
        if (typeof fields["email"] !== "undefined") {
            let lastAtPos = fields["email"].lastIndexOf('@');
            let lastDotPos = fields["email"].lastIndexOf('.');
            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["email"] = "Email is not valid";
            }
        }

        if (!this.refs.fname.value) {
            formIsValid = false;
            errors["fname"] = "Cannot be empty firstname";
        }

        if (!this.refs.message.value) {
             formIsValid = false;
            errors["message"] = "Cannot be empty username";
        }
       
        this.setState({ errors: errors });

        console.log(formIsValid);
        return formIsValid;
    }

    handleSubmit(e){

        e.preventDefault();
        if (this.handleValidation()) {

        const userInfoVo = {
            'fname': this.refs.fname.value,
            'email': this.refs.email.value,
            'message': this.refs.message.value
        }

        Reg.registerUser(userInfoVo)
            .then((result) => {
                console.log('xxx', result );
                if (result.data.success){
                    this.setState({ 
                        showAlert: true,
                        color:'success',
                        message: result.data.message
                    
                    });

                setTimeout(
                    function () {
                        this.props.history.push('/user-list');
                        this.setState({ showAlert: false });
                    }
                    .bind(this),
                    2000
                );

                }else{
                    this.setState({
                        showAlert: true,
                        color: 'warning',
                        message: result.data.message

                    });

                }
                

              }).catch(err => {
                console.log('xxx', err);
            });

        console.log('xxxxxxxx', userInfoVo );
        }
    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
    }

    render() {
        return (
            <div className="height100">

                <section id="contact_us" className="contact_us height100">
                    <div className="container custom_container height100 display_table">
                        <div className="contact_content">
                            <div className="header_section head_center text-center">
                                <h2 className="hidden-xs">Sign Up karo Beta!</h2>
                                <h2 className="visible-xs">Register interest for beta form</h2>
                            </div>
                            <div className="row">
                                <div className="cont_img">
                                    <img src="images/Register_your_interest.png" />
                                </div>
                                <div className="contact_form">
                                {this.state.showAlert ? (
                                        <Alert bsStyle={this.state.color}>
                                            <strong>{this.state.message}</strong>
                                    </Alert>

                                ):(
                                    null

                                )}
                                   
                                    <form >
                                        <div className="form-group">
                                            <label>Name</label>
                                            <input className="form-control" type="text" ref="fname"  id="fname" name="fname" value={this.state.fields["fname"]}
                                                onChange={this.handleChange.bind(this, "fname")} placeholder="Write your name here" required="" />
                                            <span style={{ color: "red" }}>{this.state.errors["fname"]}</span>
                                        </div>
                                        <div className="form-group">
                                            <label>Email Address</label>
                                            <input className="form-control" ref="email" type="email" id="email" name="email" value={this.state.fields["email"]}
                                                onChange={this.handleChange.bind(this, "email") }placeholder="Enter your email address" required="" />
                                            <span style={{ color: "red" }}>{this.state.errors["email"]}</span>
                                        </div>
                                        <div className="form-group">
                                            <label>Message</label>
                                            <textarea ref="message" value={this.state.fields["message"]}
                                                onChange={this.handleChange.bind(this, "message") }className="form-control" id="message" name="message" placeholder="Enter free text, your purpose etc." rows="6"></textarea>
                                            <span style={{ color: "red" }}>{this.state.errors["message"]}</span>
                                        </div>
                                        <div className="form-group">
                                            <input className="submit_btn btn custom_theme_btn" onClick={this.handleSubmit} type="button" value="Send" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </div>
        );
    }
}

export default MainPage;