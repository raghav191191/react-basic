import React, { Component } from 'react';
import ReactCrop from 'react-image-crop';

import { Navbar, NavItem, Nav, Modal, Button, Alert } from 'react-bootstrap';
import RegisterService from '../../serviecs/registerServiecs';
//  import j from "../../../public/upload"
// const imgUrl="../../../../api/upload-file";
const API = new RegisterService();
class Register extends Component {
    constructor(props){
        super(props);
        this.state = {
            fields: {},
            errors: {},
            show: false,
            lists:[],
            selectedFile: null
        }
        this.registerMe = this.registerMe.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.getAllEmployee = this.getAllEmployee.bind(this);
    }

    componentDidMount() {
        this.getAllEmployee();
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }

    registerMe(e){
        // const userInfoVo ={
        //     'username': this.refs.username.value,
        //     'email': this.refs.email.value,
        //     'password': this.refs.password.value
        // }
        console.log('xxxxxxxx img',  this.state.selectedFile);
        var formData = new FormData()
formData.append('image', this.state.selectedFile)
formData.append('username', this.refs.username.value)
formData.append('email', this.refs.email.value)
formData.append('password', this.refs.password.value)

        API.registerInfo(formData)
        .then((result) => {
            console.log('xxx ', result );
          
        }).catch(err => {
            console.log('xxx', err);
        }) 

    }

    getAllEmployee(){
        API.AllEmployee()
        .then(res =>{
            console.log('xxxxxxxxx xxxx result', res);
            this.setState({ lists: res.data.data });

        }).catch(err => {
            console.log('xxxxxxxxx xxxx error', err);
        })
    }

    handleChange(field, e) { 
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
    }

    fileChangedHandler = event => {
        this.setState({ selectedFile: event.target.files[0] })
        console.log('xxxxxxx image', this.state.selectedFile);
        
      }
    render() {

        return (
            <div>
                <div className="container">
                    <section>
                        <h1>Register</h1>
                        <Button bsStyle="primary" bsSize="small" onClick={this.handleShow}>
                            Add Employee
                        </Button>

                        <Modal show={this.state.show} onHide={this.handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>Modal heading</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <form >
                                    <div className="form-group">
                                        <label>User Name</label>
                                        <input className="form-control" type="text" ref="username" id="username" name="username" value={this.state.fields["username"]}
                                            onChange={this.handleChange.bind(this, "username")} placeholder="Write your username here" required="" />

                                    </div>
                                    <div className="form-group">
                                        <label>Email</label>
                                        <input className="form-control" ref="email" type="text" id="email" name="email" value={this.state.fields["email"]}
                                            onChange={this.handleChange.bind(this, "email")} placeholder="Enter your email address" required="" />
                                    </div>
                                    <div className="form-group">
                                        <label>Password</label>
                                        <input className="form-control" ref="password" type="password" id="password" name="password" value={this.state.fields["password"]}
                                            onChange={this.handleChange.bind(this, "password")} placeholder="Enter your password" required="" />
                                        {/* <span style={{ color: "red" }}>{this.state.errors["email"]}</span> */}
                                    </div>
                                    
  <div className="form-group">    
  <input type="file" onChange={this.fileChangedHandler}/>
  </div>                              <div className="form-group">
                                        <input className="submit_btn btn custom_theme_btn" onClick={this.registerMe} type="button" value="Send" />
                                    </div>
                                </form>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button onClick={this.handleClose}>Close</Button>
                            </Modal.Footer>
                        </Modal>
                       
                    </section>
                    <section>
                        <table className="table">
                            <thead>
                                <th>#Id</th>
                                <th>Image</th>
                                <th>UserName</th>
                                <th>Email</th>
                              
                            </thead>
                            <tbody>
                                {this.state.lists.map((list, index) =>
                                    <tr key={index}>
                                        <td>{index}</td>
                                          <td><img className="img-box" src={"upload/"+list.image}/></td>  
                                       
                                        <td>{list.username}</td>
                                        <td>{list.email}</td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                        
                    </section>
                </div>

            </div>
        );
    }
}

export default Register;