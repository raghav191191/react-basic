import React, { Component } from 'react';
import AuthService from '../../serviecs/AuthService';
const Auth = new AuthService();
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {},
            errors: {},
        }
        this.LoginMe = this.LoginMe.bind(this);
      
    }

    componentDidMount() {
       
    }

    LoginMe(e) {
        const userInfoVo = {
            'username': this.refs.username.value,
            'password': this.refs.password.value
        }

        console.log('xxxxxxxxxxxx login', userInfoVo);

        Auth.login(userInfoVo)
            .then((result) => {
                console.log('xxx ', result);
            }).catch(err => {
                console.log('xxx', err);
            })

    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
    }

    render() {
        return (
            <div>
              <div className="container">
                    <section>
                        <h1>Login</h1>
                        <form >
                            <div className="form-group">
                                <label>User Name</label>
                                <input className="form-control" type="text" ref="username" id="username" name="username" value={this.state.fields["username"]}
                                    onChange={this.handleChange.bind(this, "username")} placeholder="Write your username here" required="" />

                            </div>
                            <div className="form-group">
                                <label>Password</label>
                                <input className="form-control" ref="password" type="password" id="password" name="password" value={this.state.fields["password"]}
                                    onChange={this.handleChange.bind(this, "password")} placeholder="Enter your password" required="" />
                                {/* <span style={{ color: "red" }}>{this.state.errors["email"]}</span> */}
                            </div>

                            <div className="form-group">
                                <input className="submit_btn btn custom_theme_btn" onClick={this.LoginMe} type="button" value="Send" />
                            </div>
                        </form>
                    </section>
                </div>

            </div>
        );
    }
}

export default Login;