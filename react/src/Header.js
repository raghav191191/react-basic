import React, { Component } from 'react';
import { Navbar, NavItem, Nav, Alert } from 'react-bootstrap'; 
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
 import Scrollchor from 'react-scrollchor';
class Header extends Component {
    render() {
        return (
            <div>
                <header id="header" className="App-header" data-spy="affix" data-offset-top="50">
                    <Navbar inverse collapseOnSelect >
                        <Navbar.Header>
                            <Navbar.Brand>
                                <Scrollchor to="#hero_banner" className="nav-link navbar-brand"><img src="images/logo.png" /></Scrollchor>

                            </Navbar.Brand>
                            <Navbar.Toggle />
                        </Navbar.Header>
                        <Navbar.Collapse>
                            <Nav pullRight>
                                <li>
                                <NavLink to="/" className="nav-link">Add User</NavLink>
                                </li>
                                <li>
                                <NavLink to="/user-list" className="nav-link">User List</NavLink>
                                </li>
                                <li>
                                <NavLink to="/login" className="nav-link">Login</NavLink>
                                </li>
                                <li>
                                <NavLink to="/register" className="nav-link">Register</NavLink>
                                </li>
                                <li>
                                <NavLink to="/logout" className="nav-link">Logout</NavLink>
                                </li>
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>

                </header>

            </div>
        );
    }
}

export default Header;