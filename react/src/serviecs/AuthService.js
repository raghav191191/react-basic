import axios from 'axios';
export default class AuthService {
    constructor() {
        this.domain = 'http://localhost:3300'; // API server domain
    }

    login(username, password) {
       
        return axios.post(this.domain + '/api/authenticate', {
            email: username,
            pass: password
        })
            .then(result => {
                console.log(result);
                this.setToken(result.data.body);
                return Promise.resolve(result);
               
            });
    }

}