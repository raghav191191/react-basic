import React, { Component } from 'react';
import { Navbar, NavItem, Nav, NavLink } from 'react-bootstrap';
import Scrollchor from 'react-scrollchor';
class Footer extends Component {
    render() {
        return (
           
                <footer id="footer">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="display_table footer_main">
                                    <div className="footer_left">
                                        <div className="footer_logo">
                                        
                                            <a href="#"><img src="images/logo.png" /></a> <span>&copy; 2018 nestb.com</span>
                                        </div>
                                    </div>
                                    <div className="footer_right">
                                         {/* <ul className="footer_menu list-inline">
                                            <li><a href="#platform">Platform</a></li>
                                            {/* <li><a href="#blogs">Blogs</a></li> 
                                            <li><a href="#about_us">About Us</a></li>
                                            <li><a href="#contact_us">Contact Us</a></li>
                                        </ul>  */}
                                   
                                   
                                    <Nav className="footer_menu list-inline pull-right">
                                            <NavItem ><Scrollchor to="#about_us" className="nav-link">About Us</Scrollchor></NavItem>
                                            <NavItem><Scrollchor to="#platform" className="nav-link">Platform</Scrollchor></NavItem>
                                            {/* <NavItem><Scrollchor to="#blogs" className="nav-link">Blogs</Scrollchor></NavItem> */}
                                            <NavItem ><Scrollchor to="#contact_us" className="nav-link">Contact Us</Scrollchor></NavItem>
                                        </Nav>
                                  
                                  
                                   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

           
        );
    }
}

export default Footer;