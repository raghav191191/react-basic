const express = require('express');
const router = express.Router();
const userCon = require('../controller/userCon'); 
const registerCon = require('../controller/registerCon'); 

router.get('/getUser', userCon.getUserTODb);

router.post('/addUser', userCon.addUserTODb);

router.delete('/delUser/:id', userCon.delUserTODb);

router.post('/editUser', userCon.editUserTODb);

router.get('/searchData/:fname', userCon.searchUserTODb);


router.post('/addUserInfo', registerCon.addRegisterTODb); 

router.get('/getEmpList', registerCon.getEmpListTODb); 




module.exports = router; 